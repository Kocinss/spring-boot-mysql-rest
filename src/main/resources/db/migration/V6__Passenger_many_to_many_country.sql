CREATE TABLE passenger_visited_countries (
  employee_id bigint NOT NULL,
  project_id bigint NOT NULL,
  PRIMARY KEY (employee_id,project_id),
  KEY FK149cavutm2y7cmcx8m3lallub (project_id),
  CONSTRAINT FK149cavutm2y7cmcx8m3lallub FOREIGN KEY (project_id) REFERENCES countries (id),
  CONSTRAINT FKgaol0351mi7womg68t1sbmf24 FOREIGN KEY (employee_id) REFERENCES pax (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci