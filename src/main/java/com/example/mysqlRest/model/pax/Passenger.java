package com.example.mysqlRest.model.pax;

import com.example.mysqlRest.model.AbstractModel;
import com.example.mysqlRest.model.Country;
import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.enums.Language;
import com.example.mysqlRest.model.enums.PaxStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "pax",
		uniqueConstraints =
		@UniqueConstraint(columnNames = {"email"}))
@Data
@EqualsAndHashCode(callSuper=true)
public class Passenger extends AbstractModel {
	@Enumerated(EnumType.STRING)
	private Language language;

	@Enumerated(EnumType.STRING)
	private Gender gender;
	@Email
	@Access(AccessType.PROPERTY)
	private String email;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	private PaxStatus status;

	@NotNull
	@Valid
	@OneToOne(mappedBy = "passenger", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private PaxName name;
	@Valid
	@OneToOne(mappedBy = "passenger", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private PaxLogin login;
	@Valid
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "passenger", orphanRemoval = true)
	private List<PaxAddress> addresses = new ArrayList<>();
	@Valid
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "passenger", orphanRemoval = true)
	private List<PaxPhone> phones = new ArrayList<>();

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "passenger_visited_countries",
			joinColumns = { @JoinColumn(name = "employee_id") },
			inverseJoinColumns = { @JoinColumn(name = "project_id") }
	)
	Set<Country> visitedCountries = new HashSet<>();
}
