package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.pax.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    @Query("FROM Passenger p " +
            "join p.addresses address WHERE address.type.code=(:addressTypeCode)")
    List<Passenger> findAddressTypeCode(@Param("addressTypeCode") String addressTypeCode);

    @Query("FROM Passenger p " +
            "p.gender WHERE gender =(:F)")
    List<Passenger> FindGenderTypeCode(@Param("gender") String FindGender);

}


