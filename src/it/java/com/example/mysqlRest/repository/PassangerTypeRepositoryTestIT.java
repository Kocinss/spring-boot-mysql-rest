package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.pax.Passenger;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PassangerTypeRepositoryTestIT {
    @Autowired
    PassengerRepository passengerRepository;

    @Test
    @Transactional
    void adresstest() {
        List<Passenger> passengerList = passengerRepository.findAddressTypeCode("H");
        log.info("H", passengerList.size());
        passengerList.get(0).getAddresses();
//        assertEquals("H",passengerList.get(0): size= 369);
    }

    @Test
    @Transactional
    void findByGender() {
        List<Passenger> gender = passengerRepository.FindGenderTypeCode("Gender.F");
        assertTrue(gender.size() > 0);
        assertEquals(Gender.F, gender.get(0).getGender());
    }
}

