package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.AddressType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class AddressTypeRepositoryTestIT{
    @Autowired
    AddressTypeRepository addressTypeRepository;
    @Test
    void findByCode(){
        AddressType h = addressTypeRepository.findByCode("H");
        log.info((h.getName()));
        assertEquals("H",h.getCode());
    }



}